# product
class Comment
  def initialize(user, content)
    @user    = user
    @content = content
  end

  def output
    puts "#{@user}さんにコメント：#{@content}"
  end
end

class Mail
  def initialize(user, content)
    @user    = user
    @content = content
  end

  def output
    puts "#{@user}さんにメール：#{@content}"
  end
end

# creator
class ContactFactory
  def initialize(users)
    @contacts = []
    users.each_with_index do |user, i|
      contact = new_contact(user, "コンタクト#{i}")
      @contacts << contact
    end
  end

  def output
    @contacts.each{|contacts| contacts.output}
  end
end

# concrete creator
class CommentFactory < ContactFactory
  def new_contact(user, content)
    Comment.new(user, content)
  end
end

# concrete creator
class MailFactory < ContactFactory
  def new_contact(user, content)
    Mail.new(user, content)
  end
end


comment_factory = CommentFactory.new(["太郎", "次郎", "三郎"])
comment_factory.output

mail_factory = MailFactory.new(["花子", "洋子", "早紀"])
mail_factory.output
