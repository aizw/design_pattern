class PantsSuit
  def initialize(user)
    @user = user
  end

  def wear
    puts "#{@user}さんはパンツスーツを着ています。"
  end
end

class SkirtSuit
  def initialize(user)
    @user = user
  end

  def wear
    puts "#{@user}さんはスカートスーツを着ています。"
  end
end

class LeatherShoes
  def initialize(user)
    @user = user
  end

  def wear
    puts "#{@user}さんは革靴を履いています。"
  end
end

class Pumps
  def initialize(user)
    @user = user
  end

  def wear
    puts "#{@user}さんはパンプスを履いています。"
  end
end

class MensAppearanceFactory
  def new_cloth(user)
    PantsSuit.new(user)
  end

  def new_shoes(user)
    LeatherShoes.new(user)
  end
end

class LediesAppearanceFactory
  def new_cloth(user)
    SkirtSuit.new(user)
  end

  def new_shoes(user)
    Pumps.new(user)
  end
end

class Appearance
  def initialize(appearance_factory, user)
    @appearance_factory = appearance_factory
    @user = user

    @cloth = @appearance_factory.new_cloth(@user)
    @shoes = @appearance_factory.new_shoes(@user)
  end

  def wear
    @cloth.wear
    @shoes.wear
  end
end

mens = Appearance.new(MensAppearanceFactory.new, "太郎")
mens.wear

ledies = Appearance.new(LediesAppearanceFactory.new, "花子")
ledies.wear

# Rfc
class AppearanceFactory
  def initialize(cloth_class, shoes_class)
    @cloth_class = cloth_class
    @shoes_class = shoes_class
  end

  def new_cloth(user)
    @cloth_class.new(user)
  end

  def new_shoes(user)
    @shoes_class.new(user)
  end
end

mens_appearance_factory =  AppearanceFactory.new(PantsSuit, LeatherShoes)
mens = Appearance.new(mens_appearance_factory, "次郎")
mens.wear
