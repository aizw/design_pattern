# パーサーを作らないパターン
## 命令・抽象的な表現 AbstractExpression
class Expression
  def |(other)
    Or.new(self, other)
  end

  def &(other)
    And.new(self, other)
  end
end

# 終端となる表現(構造木の葉) TerminalExpression
class All < Expression
  def evaluate(tasks)
    tasks
  end
end

# 終端となる表現(構造木の葉) TerminalExpression
class Title < Expression
  def initialize(pattern)
    @pattern = pattern
  end

  def evaluate(tasks)
    results = []

    tasks.each do |task|
      results << task if task.title.include?(@pattern)
    end

    results
  end
end

# 終端となる表現(構造木の葉) TerminalExpression
class Heavy < Expression
  def initialize(time)
    @time = time
  end

  def evaluate(tasks)
    results = []

    tasks.each do |task|
      results << task if task.required_time >= @time
    end

    results
  end
end

# 終端以外の表現(構造木の節) NonterminalExpression
class Not < Expression
  def initialize(expression)
    @expression = expression
  end

  def evaluate(tasks)
    All.new.evaluate(tasks) - @expression.evaluate(tasks)
  end
end

# 終端以外の表現(構造木の節) NonterminalExpression
class Or
  def initialize(expression1, expression2)
    @expression1 = expression1
    @expression2 = expression2
  end

  def evaluate(tasks)
    result1 = @expression1.evaluate(tasks)
    result2 = @expression2.evaluate(tasks)
    (result1 + result2).uniq
  end
end

# 終端以外の表現(構造木の節) NonterminalExpression
class And
  def initialize(expression1, expression2)
    @expression1 = expression1
    @expression2 = expression2
  end

  def evaluate(tasks)
    result1 = @expression1.evaluate(tasks)
    result2 = @expression2.evaluate(tasks)
    (result1 & result2)
  end
end

class Task
  attr_reader :title, :required_time

  def initialize(title, required_time)
    @title = title
    @required_time = required_time
  end
end

tasks = [
  Task.new("デザパタ勉強会準備", 30),
  Task.new("勤怠開発", 100),
  Task.new("メール確認", 5),
  Task.new("通知確認", 5),
  Task.new("レビュー対応", 100),
]

p "--------所要時間が100以上のタスク---------------------"
p Heavy.new(100).evaluate(tasks).inspect

p "--------所要時間が100以上かつタイトルに「開発」が含まれているタスク---------------------"
p (Heavy.new(100) & Title.new("開発")).evaluate(tasks).inspect

p "--------所要時間が100以上またはタイトルに「通知」が含まれているタスク---------------------"
p (Heavy.new(100) | Title.new("通知")).evaluate(tasks).inspect

p "--------所要時間が100よりも小さいタスク---------------------"
p Not.new(Heavy.new(100)).evaluate(tasks).inspect
