# パーサーを作るパターン
class Parser
  def initialize(text)
    @text = text
  end

  def parse
    context = Context.new(@text)

    while context.current_token != "end"
      context.expression
    end

    nil
  end
end

class Context
  def initialize(text)
    @tokens = text.split(" ")
  end

  def next_token
    @current_token = @tokens.shift
    return @current_token
  end

  def current_token
    return @current_token
  end

  def expression
    token = next_token

    case token
    when nil
      nil
    when "(", ")"
      expression
    when "and"
      And.new(expression, expression).evaluate
    when "or"
      Or.new(expression, expression).evaluate
    when "create"
      Create.new(next_token)
    when "delete"
      Delete.new(next_token)
    when "end"
      "end"
    end
  end
end

class Expression
  def initialize(task)
    @task = task
  end
end

class Delete < Expression
  def execute
    "#{@task}を削除"
  end
end

class Create < Expression
  def execute
    "#{@task}を作成"
  end
end

class Or
  def initialize(expression1, expression2)
    @expression1 = expression1
    @expression2 = expression2
  end

  def evaluate
    p "#{@expression1.execute} または #{@expression2.execute}"
  end
end

class And
  def initialize(expression1, expression2)
    @expression1 = expression1
    @expression2 = expression2
  end

  def evaluate
    p "#{@expression1.execute} かつ #{@expression2.execute}"
  end
end

p = Parser.new("and ( create デザパタ勉強会 ) ( delete レビュー対応 ) or ( create 進捗チェック ) ( create 資料制作 ) end")
p.parse
