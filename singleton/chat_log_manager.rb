require "singleton"

class ChatLogManager
  include Singleton
  attr_accessor :log

  def get_log
    @log = ""
  end
end

class Chat
  def initialize
    @reader = Reader.new
    @writer = Writer.new
  end

  def write(log)
    @writer.write(ChatLogManager.instance, log)
  end

  def get_log
    @reader.read(ChatLogManager.instance)
  end
end

class Writer
  def write(chat_log, message)
    chat_log.log += message + "\n"
  end
end

class Reader
  def read(chat_log)
    puts chat_log.log
  end
end

log = ChatLogManager.instance
log.log = "ほげ"
puts log.log

log2 = ChatLogManager.instance
log.log += "ほげ"
puts log.log

p "-----------------"

chat = Chat.new
chat.write("ありがとうございます！> デザパタ男さん")
chat.write("どういたしまして〜")
chat.get_log

p "-----------------"

chat2 = Chat.new
chat.write("そういえばあの件どうなりました？> デザパタ男さん")
chat.write("どの件ですか？")
chat.get_log
