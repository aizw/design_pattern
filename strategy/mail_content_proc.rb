class MailContent
  attr_reader :address, :texts, :name

  def initialize(address, &formatter)
    @address   = address
    @formatter = formatter
    @name      = "デザ パタ夫"
    @texts     = [
      "先日ご相談させていただきました内容に関して、進捗はいかがでしょうか？",
      "恐れ入りますが、ご確認いただけますと幸いです。"
    ]
  end

  def output_content
    @formatter.call(self)
  end
end

internal_mail_content = MailContent.new("田中") do |context|
  puts "#{context.address}さん"
  puts ""
  puts "お疲れ様です。#{context.name}です。"
  puts ""

  context.texts.each do |text|
    puts text
  end

  puts ""
  puts "どうぞよろしくお願いいたします。"
  puts ""

  puts <<-EOS
-----------------
株式会社デザパタ
営業部
#{context.name}
-----------------
  EOS
end

internal_mail_content.output_content

external_mail_content = MailContent.new("坂本") do |context|
  puts "#{context.address}様"
  puts ""
  puts "お世話になっております。デザパタの#{context.name}です。"
  puts ""

  context.texts.each do |text|
    puts text
  end

  puts ""
  puts "どうぞよろしくお願いいたします。"
  puts ""

  puts <<-EOS
-----------------
株式会社デザパタ
営業部
#{context.name}
-----------------
  EOS
end

external_mail_content.output_content
