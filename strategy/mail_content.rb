class MailContent
  attr_reader :address, :texts, :name
  attr_accessor :formatter

  def initialize(address, formatter)
    @address   = address
    @formatter = formatter
    @name      = "デザ パタ夫"
    @texts     = [
      "先日ご相談させていただきました内容に関して、進捗はいかがでしょうか？",
      "恐れ入りますが、ご確認いただけますと幸いです。"
    ]
  end

  def output_content
    @formatter.output_content(self)
  end
end

class InternalMailContentFormatter
  def output_content(context)
    puts "#{context.address}さん"
    puts ""
    puts "お疲れ様です。#{context.name}です。"
    puts ""

    context.texts.each do |text|
      puts text
    end

    puts ""
    puts "どうぞよろしくお願いいたします。"
    puts ""

    puts <<-EOS
-----------------
株式会社デザパタ
営業部
#{context.name}
-----------------
    EOS
  end
end

class ExternalMailContentFoematter
  def output_content(context)
    puts "#{context.address}様"
    puts ""
    puts "お世話になっております。デザパタの#{context.name}です。"
    puts ""

    context.texts.each do |text|
      puts text
    end

    puts ""
    puts "どうぞよろしくお願いいたします。"
    puts ""

    puts <<-EOS
-----------------
株式会社デザパタ
営業部
#{context.name}
-----------------
    EOS
  end
end

tanaka_mail = MailContent.new("田中", InternalMailContentFormatter.new)
tanaka_mail.output_content
tanaka_mail.formatter = ExternalMailContentFoematter.new
tanaka_mail.output_content
