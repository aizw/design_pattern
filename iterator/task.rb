class Task
  attr_accessor :title, :required_time

  def initialize(title, required_time)
    @title = title
    @required_time = required_time
  end
end

class TaskList
  include Enumerable
  attr_accessor :title

  def initialize(title)
    @title = title
    @tasks = []
  end

  def each(&block)
    @tasks.each(&block)
  end

  def add_task(task)
    @tasks << task
  end
end


task_list = TaskList.new('今日対応すること')
task_list.add_task(Task.new('デザパタ勉強会準備', 15))
task_list.add_task(Task.new('開発プロジェクト', 60))

p task_list.title
p "タスク"
task_list.each {|task| p "・" + task.title }

if task_list.any? {|task| task.required_time > 60 }
  p "※60分以上かかるタスクがあります。"
else
  p "※60分以上かかるタスクはありません。"
end
