module Subject
  def initialize
    @observers = []
  end

  def add_observer(observer)
    @observers << observer
  end

  def delete_observer(observer)
    @observers.delete(observer)
  end

  def notify_observers_for_update_girl_friend
    @observers.each do |observer|
      observer.update_girl_friend(self)
    end
  end

  def notify_observers_for_update_marrid
    @observers.each do |observer|
      observer.update_marrid(self)
    end
  end
end

class PlayBoy
  include Subject

  attr_reader :name, :girl_friend, :marrid

  def initialize(name, girl_friend, marrid)
    super()

    @name        = name
    @girl_friend = girl_friend
    @marrid      = marrid
  end

  def girl_friend=(new_girl_friend)
    old_girl_frend = @girl_friend
    @girl_friend   = new_girl_friend

    if old_girl_frend != new_girl_friend
      notify_observers_for_update_girl_friend
    end
  end

  def marrid=(marrid)
    old_marrid = @marrid
    @marrid    = marrid

    if old_marrid != marrid
      notify_observers_for_update_marrid
    end
  end
end

class Rumor
  def update_girl_friend(changed_play_boy)
    if changed_play_boy.girl_friend
      puts "#{changed_play_boy.name}の彼女がかわったらしいよ。新しい彼女は#{changed_play_boy.girl_friend}だよ！"
    else
      puts "#{changed_play_boy.name}が彼女と別れたってよ！"
    end
  end

  def update_marrid(changed_play_boy)
    if changed_play_boy.marrid
      puts "#{changed_play_boy.name}が彼女と結婚したらしいよ。奥さんは#{changed_play_boy.girl_friend}だよ！"
    else
      puts "#{changed_play_boy.name}が#{changed_play_boy.girl_friend}と離婚したってよ...！"
    end
  end
end

class Bless
  def update_girl_friend(changed_play_boy)
    if changed_play_boy.girl_friend
      puts "#{changed_play_boy.name}、#{changed_play_boy.girl_friend}と付き合い始めたんだってね、おめでとう!!"
    else
      puts "#{changed_play_boy.name}、彼女と別れたんだね。#{changed_play_boy.name}ならまたいい出会いがあるよ！"
    end
  end

  def update_marrid(changed_play_boy)
    if changed_play_boy.marrid
      puts "#{changed_play_boy.name}、結婚おめでとう！#{changed_play_boy.girl_friend}と幸せになってね。"
    else
      puts "#{changed_play_boy.name}、#{changed_play_boy.girl_friend} と離婚したんだね...。辛いと思うけど、今度おいしいものでも食べにいこう！"
    end
  end
end

boy = PlayBoy.new("太郎", "まりえ", false)
rumor_observer = Rumor.new
bless_observer = Bless.new

boy.add_observer(rumor_observer)

puts "---------Rumor---------------"

boy.girl_friend = "はるか"
boy.marrid = true
boy.marrid = false
boy.girl_friend = "すみれ"
boy.girl_friend = "さき"

boy.delete_observer(rumor_observer)
boy.add_observer(bless_observer)

puts "---------Bless---------------"

boy.girl_friend = "はるか"
boy.marrid = true
boy.marrid = false
boy.girl_friend = "すみれ"
boy.girl_friend = "さき"

