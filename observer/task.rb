NOTIFICATIONS = []

module Subject
  def initialize
    @observers = []
  end

  def add_observer(observer)
    @observers << observer
  end

  def delete_observer(observer)
    @observers.delete(observer)
  end

  def notify_observers
    @observers.each do |observer|
      observer.update(self)
    end
  end
end

class Task
  include Subject

  attr_reader :title, :content

  def initialize(title, content)
    super()

    @title   = title
    @content = content
  end

  def content=(new_content)
    old_content = @content
    @content    = new_content

    if old_content != new_content
      notify_observers
    end
  end
end

class Notify
  def initialize(user)
    @user = user
  end

  def update(changed_task)
    notification = Notification.find_or_new(@user)
    notification.add_content("タスクの内容が「#{changed_task.content}」に変わりました。")
  end
end

class Notification
  attr_reader :user, :contents

  def initialize(user)
    @user = user
    @contents = []

    NOTIFICATIONS << self
  end

  def self.find(user)
    NOTIFICATIONS.find{|notification| notification.user == user}
  end

  def self.find_or_new(user)
    NOTIFICATIONS.find{|notification| notification.user == user} || self.new(user)
  end

  def add_content(content)
    @contents << content
  end
end

task = Task.new("デザパタ勉強会の準備", "通知のパターンを考える！")

user1 = "鶴男"
user2 = "絹江"

notify1 = Notify.new(user1)
notify2 = Notify.new(user2)

task.add_observer(notify1)
task.add_observer(notify2)

task.content = "通知のパターンを考えて発表の準備をする！"
task.content = "通知のパターンを考えて発表の準備をしてサンプルコードも書く！"

puts "#{user1}さんの通知内容"
Notification.find(user1).contents.each{|notification_content| puts notification_content }
puts "#{user2}さんの通知内容"
Notification.find(user2).contents.each{|notification_content| puts notification_content }
