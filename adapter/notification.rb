class Notification
  def notify(obj)
    puts "・" + obj.humanized_class_name
    puts "#{obj.title}：#{obj.description}"
  end
end

class Task
  attr_reader :title, :description

  def initialize(title, description)
    @title = title
    @description = description
  end

  def humanized_class_name
    "タスク"
  end
end

class TaskMail
  attr_reader :subject, :content

  def initialize(subject, content)
    @subject = subject
    @content = content
  end
end

class Todo
  attr_reader :title, :content

  def initialize(title, content)
    @title = title
    @content = content
  end
end

class TaskMailAdapter < Task
  def initialize(mail)
    @mail = mail
  end

  def title
    @mail.subject
  end

  def description
    @mail.content
  end

  def humanized_class_name
    "タスクメール"
  end
end

notification = Notification.new
task = Task.new('コードレビュー', "太郎さんの開発した内容のコードを確認する。")
notification.notify(task)

task_mail = TaskMail.new('日報確認', 'ユニットメンバーの日報を確認する。')
notification.notify(TaskMailAdapter.new(task_mail))

todo = Todo.new('お薬を飲む', "毎食後20分以内に服用すること。")

class << todo
  def humanized_class_name
    "TODO"
  end

  def description
    content
  end
end

notification.notify(todo)
