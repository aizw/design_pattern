# compornent
class Entry
  attr_reader :name
  attr_accessor :parent

  def initialize(name)
    @name   = name
    @parent = nil
  end

  def get_price
  end
end

# leaf
class HouseholdAccountBookItem < Entry
  attr_reader :price

  def initialize(name, price)
    super(name)
    @price = price
  end

  def get_price
    @price
  end
end

# composite
class HouseholdAccountBookCategory < Entry
  def initialize(name)
    super(name)
    @categories = []
  end

  def <<(entry)
    @categories << entry
    entry.parent = self
  end

  def delete(entry)
    @categories.delete(entry)
    entry.parent = nil
  end

  def get_price
    @categories.inject(0) do |total_price, category|
      total_price += category.get_price
    end
  end
end

item  = HouseholdAccountBookItem.new("昼食", 1000)
item2 = HouseholdAccountBookItem.new("夕食", 5000)
item3 = HouseholdAccountBookItem.new("美容院", 5000)
item4 = HouseholdAccountBookItem.new("タクシー", 1500)
item5 = HouseholdAccountBookItem.new("出張費", 20000)

category  = HouseholdAccountBookCategory.new("食費")
category2 = HouseholdAccountBookCategory.new("プライベート")
category3 = HouseholdAccountBookCategory.new("ビジネス")

category << item
category << item2

category2 << item3
category2 << category

category3 << item4
category3 << item5

puts "#{category.name}カテゴリは#{category.get_price}円。"  + (category.parent ? "#{category.parent.name}カテゴリに入っています。" : "カテゴリには入っていません。")
puts "#{category2.name}カテゴリは#{category2.get_price}円。"  + (category2.parent ? "#{category2.parent.name}カテゴリに入っています。" : "カテゴリには入っていません。")
puts "#{category3.name}カテゴリは#{category3.get_price}円。"  + (category3.parent ? "#{category3.parent.name}カテゴリに入っています。" : "カテゴリには入っていません。")
