class MailContent
  def initialize(address)
    @address = address
    @texts = [
      "先日ご相談させていただきました内容に関して、進捗はいかがでしょうか？",
      "恐れ入りますが、ご確認いただけますと幸いです。"
    ]
    @name = "デザ パタ夫"
  end

  def output_content
    address
    blank_line
    greeting
    blank_line
    main_contents
    blank_line
    end_greeting
    blank_line
    signature
  end

  private

  def address
    raise NotImplementedError
  end

  def greeting
    raise NotImplementedError
  end

  def main_contents
    @texts.each do |text|
      puts text
    end
  end

  def end_greeting
    puts "どうぞよろしくお願いいたします。"
  end

  def signature
    puts <<-EOS
-----------------
株式会社デザパタ
営業部
#{@name}
-----------------
    EOS
  end

  def blank_line
    puts ""
  end
end

class InternalMailContent < MailContent
  private

  def address
    puts "#{@address}さん"
  end

  def greeting
    puts "お疲れ様です。#{@name}です。"
  end
end

class ExternalMailContent < MailContent
  private

  def address
    puts "#{@address}様"
  end

  def greeting
    puts "お世話になっております。株式会社デザパタの#{@name}です。"
  end
end

InternalMailContent.new("田中").output_content
ExternalMailContent.new("坂本").output_content
