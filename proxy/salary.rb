class Salary
  attr_reader :user_name, :salary

  def initialize(user_name, salary)
    @user_name = user_name
    @salary = salary
  end

  def increase(price)
    @salary += price
  end

  def decrease(price)
    @salary -= price
  end
end

# 防御proxy
class SalaryProxy
  def initialize(object, login_name)
    @object = object
    @login_name = login_name
  end

  def increase(price)
    authorized?
    @object.increase(price)
  end

  def decrease(price)
    authorized?
    @object.decrease(price)
  end

  def salary
    @object.salary
  end

  def authorized?
    if @login_name != @object.user_name
      raise "Illegal access: #{@login_name} cannot access name."
    end
  end
end

salary = Salary.new("たかし", 1000)
salary2 = Salary.new("ひろし", 1000)

proxy = SalaryProxy.new(salary, "たかし")
proxy.increase(600)
puts proxy.salary

proxy2 = SalaryProxy.new(salary2, "たかし")
#proxy2.decrease(600) # error

# 仮想プロキシ

class VirtualSalaryProxy
  def initialize(&block)
    @block = block
  end

  def increase(price)
    subject.increase(price)
  end

  def decrease(price)
    subject.decrease(price)
  end

  def salary
    subject.salary
  end

  def subject
    @subject = @block.call unless @subject
    @subject
  end
end

proxy = VirtualSalaryProxy.new{ Salary.new("小春", 3000) }
proxy.increase(600)
puts proxy.salary


