class Inquiry
  def initialize(destination)
    @destination = destination
  end

  def request_president_schedule
    puts "問合せ：明日の社長の出社予定は？"
    @destination.answer_president_schedule
  end

  def request_president_appoint
    puts "問合せ：明日の10時から1時間社長とMTGをしたいです。"
    @destination.answer_president_appoint
  end

  def request_official_judge
    puts "問合せ：有休の日数を100日にしてほしいです！"
    @destination.answer_official_judge
  end
end

class President
  def answer_official_judge
    puts "#{self.class.name}：だめです"
  end
end

class Secretary
  def initialize(president)
    @president = president
  end

  def answer_president_schedule
    puts "#{self.class.name}：本日は9:00出社です。"
  end

  def answer_president_appoint
    puts "#{self.class.name}：OKです！"
  end

  def answer_official_judge
    puts "#{self.class.name}：私では判断できないので社長に確認します"
    @president.answer_official_judge
  end
end

secretary = Secretary.new(President.new)
inquiry = Inquiry.new(secretary)
inquiry.request_president_schedule
inquiry.request_president_appoint
inquiry.request_official_judge

