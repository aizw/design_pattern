class Character
  attr_accessor :name, :characteristic

  def initialize(name, characteristic)
    @name = name
    @characteristic = characteristic
  end
end

class Cart
  attr_accessor :name, :power

  def initialize(name, power)
    @name = name
    @power = power
  end
end

class Item
  attr_accessor :name, :hit_point

  def initialize(name, hit_point)
    @name = name
    @hit_point = hit_point
  end
end

class Race
  attr_accessor :character, :cart, :items

  def initialize(character = "", cart = "", items = [])
    @character = character
    @cart = cart
    @items = items
  end

  def ready
    puts "#{@character.characteristic}#{@character.name}を、パワー#{@cart.power}の#{@cart.name}で#{@items.map{|item| %Q(攻撃力#{item.hit_point}の#{item.name}) }.join("、")}のアイテムを搭載してレースに望みます！"
    puts "○(*ゝω・´)o {頑張れー！"
  end
end

class RaceBuilder
  attr_reader :race

  def initialize
    @race = Race.new
  end

  def add_banana_item
    @race.items << Item.new("バナナ", "5")
  end

  def add_shell_item
    @race.items << Item.new("甲羅", "20")
  end

  def build
    set_character
    set_cart
    add_banana_item
    add_shell_item
    @race
  end
end

class KinopioRaceBuilder < RaceBuilder
  def set_character
    @race.character = Character.new("キノピオ", "小回りがきく")
  end

  def set_cart
    @race.cart = Cart.new("キノピオのカート", "100")
  end
end

class KuppaRaceBuilder < RaceBuilder
  def set_character
    @race.character = Character.new("クッパ", "でかくて重い")
  end

  def set_cart
    @race.cart = Cart.new("クッパのカート", "100")
  end
end

builder = KinopioRaceBuilder.new
builder.build.ready

builder = KuppaRaceBuilder.new
builder.build.ready
