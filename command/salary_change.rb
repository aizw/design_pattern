class User
  attr_accessor :salary
  attr_reader :name

  def initialize(name, salary)
    @name  = name
    @salary = salary
  end
end

class Command
  attr_accessor :description

  def initialize(description)
    @description = description
  end

  def execute
    raise NotImplementedError
  end
end

class SalaryIncrease < Command
  def initialize(user, price)
    @user = user
    @price = price
  end

  def execute
    @user.salary += @price
  end

  def unexecute
    @user.salary -= @price
  end

  def description
    "#{@user.name}の給与がアップしました😊"
  end
end


class SalaryDecrease
  def initialize(user, price)
    @user = user
    @price = price
  end

  def execute
    @user.salary -= @price
  end

  def unexecute
    @user.salary += @price
  end

  def description
    "#{@user.name}の給与が減りました😭"
  end
end

class SalaryDouble
  def initialize(user)
    @user = user
  end

  def execute
    @user.salary *= 2
  end

  def unexecute
    @user.salary /= 2
  end

  def description
    "#{@user.name}の給与が2倍になりました😂✨💕💕"
  end
end

class CompositeCommand < Command
  def initialize
    @commands = []
  end

  def add_command(cmd)
    @commands << cmd
  end

  def execute
    @commands.each{|cmd| cmd.execute}
  end

  def description
    @commands.inject(""){|description, cmd| description += cmd.description + "\n"}
  end
end


cmds = CompositeCommand.new
user = User.new('太郎', 150000)

cmds.add_command(SalaryIncrease.new(user, 2000))
cmds.add_command(SalaryDecrease.new(user, 10000))
cmds.add_command(SalaryDouble.new(user))

cmds.execute
puts cmds.description
