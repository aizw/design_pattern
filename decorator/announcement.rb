class Announcement
  attr_reader :title

  def initialize(title)
    @title = title
  end

  def output
    p @title
    p "---------"
  end

  def method2
  end

  def method3
  end
end

class Decorator
  def initialize(announcement)
    @announcement = announcement
  end
end

class AnnounceWithNotifyDecorator < Decorator
  def output
    p "notify."
    @announcement.output
  end
end

class AnnounceWithMailDecorator < Decorator
  def output
    p "send mail."
    @announcement.output
  end
end

class AnnounceWithTaskDecorator < Decorator
  def output
    p "create task."
    @announcement.output
  end
end

announcement = Announcement.new("デザパタ勉強会の成果報告")
announcement.output

with_notify_announcement = AnnounceWithNotifyDecorator.new(announcement)
with_notify_announcement.output

with_mail_and_notify_announcement = AnnounceWithMailDecorator.new(with_notify_announcement)
with_mail_and_notify_announcement.output

with_mail_and_notify_and_task_announcement = AnnounceWithTaskDecorator.new(with_mail_and_notify_announcement)
with_mail_and_notify_and_task_announcement.output


# モジュールを使うパターン

module AnnounceWithMailDecoratorModule
  def output
    p "send mail."
    super
  end
end

announcement = Announcement.new("モジュールを使ってみたよ")
announcement.extend(AnnounceWithMailDecoratorModule)
announcement.output

# Forwardableを使うパターン
require 'forwardable'

class AnnounceWithTaskDecorator2
  extend Forwardable

  def_delegators :@announcement, :method2, :method3

  def initialize(announcement)
    @announcement = announcement
  end

  def output
    p "create task."
    @announcement.output
  end
end

announcement = Announcement.new("Forwardable使ってみたよ")
AnnounceWithTaskDecorator2.new(announcement).output
