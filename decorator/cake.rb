class Cake
  attr_reader :name

  def initialize
    @name = "ケーキ"
  end
end

class Decorator
  def initialize(cake)
    @cake = cake
  end
end

class Chocolate < Decorator
  def name
    "チョコレート#{@cake.name}"
  end
end

class Cheese < Decorator
  def name
    "チーズ#{@cake.name}"
  end
end

puts Cake.new.name
puts Chocolate.new(Cake.new).name
puts Cheese.new(Cake.new).name
puts Chocolate.new(Cheese.new(Cake.new)).name
